var express = require('express');
var router = express.Router();
var admin_controller = require('../controllers/adminController');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('admin', { title: 'Admin' });
// });

router.get('/', admin_controller.index)

module.exports = router;