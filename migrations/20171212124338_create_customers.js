
exports.up = function(knex, Promise) {
  return knex.schema.createTable('customers', function(t) {
      t.increments('id').unsigned().primary();
      t.dateTime('createdAt').notNull();
      t.dateTime('updatedAt').nullable();
      t.dateTime('deletedAt').nullable();

      t.string('name');
      t.text('phoneNumber').notNull();
      t.boolean('matched');
      
      
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('customers');
};
