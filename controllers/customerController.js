var Customer = require('../models/customer');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var request = require('request');
var cheerio = require('cheerio');
var csvParse = require('csv-parse')

// Display list of all customers
exports.customer_list = function(req, res) {
    Customer.forge()
    .fetch()
    .then(function (collection) {
      res.json({error: false, data: collection.toJSON()});
    })
    .catch(function (err) {
      res.status(500).json({error: true, data: {message: err.message}});
    });
};

// Display detail page for a specific customer
exports.customer_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: customer detail: ' + req.params.id);
};

// Display customer create form on GET
exports.customer_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: customer create GET');
};

// Handle customer create on POST
exports.customer_create_post = function(req, res) {
	var phoneNumbers;
	var errors = [];
	var savedNumbers = [];
	var form = new formidable.IncomingForm();
	  form.parse(req, function(err, fields, files) {
      // `file` is the name of the <input> field of type `file`
      var old_path = files.file.path,
        file_size = files.file.size,
        file_ext = files.file.name.split('.').pop(),
        index = old_path.lastIndexOf('/') + 1,
        file_name = old_path.substr(index),
        new_path = path.join(process.env.PWD, '/uploads/', file_name + '.' + file_ext);

        fs.readFile(old_path, function(err, data) {
        	csvParse(data, function(err, data){
        		phoneNumbers = [].concat.apply([], data)
        		phoneNumbers.forEach(function(item, index){
        			Customer.forge({phoneNumber: item, createdAt: new Date().toISOString() }).save()
        			.then(function (customer) {
        				// res.json({error: false, data: {id: user.get('id')}});
        				savedNumbers.push({id: user.get('id')})
        				console.log(savedNumbers);
					    })
					    .catch(function (err) {
					      //res.json({error: true, data: {message: err.message}});
					      errors.push({message: err.message})
					      console.log(errors)
					    });
        		});
        		
        	});
        });

        
     });
  	res.send("check error and saved");
};

// Display customer delete form on GET
exports.customer_check_eligibility = function(req, res) {
    res.send('NOT IMPLEMENTED: customer delete GET');
};

// Display customer delete form on GET
exports.customer_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: customer delete GET');
};

// Handle customer delete on POST
exports.customer_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: customer delete POST');
};

// Display customer update form on GET
exports.customer_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: customer update GET');
};

// Handle customer update on POST
exports.customer_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: customer update POST');
};