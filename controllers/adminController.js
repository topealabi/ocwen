var Bookshelf = require('../bookshelf')
var customer = require('../models/customer');

var Customer = Bookshelf.Collection.extend({
  model: customer
});

exports.index = function(req, res) {
    Customer.forge()
    .fetch()
    .then(function (collection) {

      
      res.render('admin', { title: 'Admin', customers: collection.toJSON() });
    })
    .catch(function (err) {
      res.status(500).json({error: true, data: {message: err.message}});
    });
};