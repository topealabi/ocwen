var knex = require('knex')({
  client: 'postgresql',
  connection: {
    
    database : 'ocwen_development',
    
  }
});

module.exports = require('bookshelf')(knex);