var bookshelf = require('../bookshelf');

var Customer = bookshelf.Model.extend({
  tableName: 'customers'
});

module.exports = Customer;